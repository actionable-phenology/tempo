FROM rocker/tidyverse:4.0.2

RUN apt-get update && apt-get install -y --no-install-recommends qpdf libmagick++-dev lbzip2 libglpk-dev
RUN install2.r \
	nimble \
	tvgeom \
	doParallel \
	mvtnorm \
	abind \
	hexSticker \
	showtext

RUN R -e "update.packages(ask = FALSE)"
