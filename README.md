# tempo: Temporally Explicit Models for Phenological Observations
<img align="left" width="200" height="200" hspace="250" src="https://gitlab.com/actionable-phenology/tempo/raw/main/logo.png">


The tempo R package provides data simulation and model fitting functions for discrete time-to-event data in a Bayesian framework. The model is built on the [tvgeom distribution](https://gitlab.com/actionable-phenology/tvgeom), which models the discrete time step at which the event of interest (e.g. failure, success, etc.) occurs conditional on a vector that defines the probability of event occurrence at each time step. This model accommodates both time-dependent covariates and censoring of observations (left, right, and interval).

<br>
<br>
<br>
<br>

## Data
This package accomadates censored or exact observation data (or a mixture of both). Raw data, ***y***, for a given observation consist of 0s and 1s (y = 0, event has not happened; y = 1, event has happened) for each time step, *t*, in which there was an observation. Observations can be made through some time period, *t* = 1 to *T* (e.g. day of year: 1 to 365). The table below is derived from those data, and demonstrates the format required for this R package. *obs_id* is a unique identifier for each row,  *c<sub>1</sub>* is the last observed time step for which *y* = 0, and *c<sub>2</sub>* is the first observed time step for which *y* = 1. *NA*s are described in the text below the table.

#### Example Data

|obs_id| c<sub>1</sub>               | c<sub>2</sub>           | 
|:----| -----------------: |-------------:| 
|1|   5                 |        6  |
|2|  3               |       7  |
|3|  *NA*           | 16        |
|4| 2            |        *NA* |

Row one is an example of an exact observation. The last observed 0 occurred at *t* = 5, and the first observed 1 occurred at *t* = 6, so the event must have occured at *t* = 6.

Row two is an example of interval censoring. The exact time at which the event occurred is unknown, but it is known that the event must have happened at *t* = 4, 5, 6, or 7.

The third row is an example of left censoring. In this case, *y* = 0 was never observed, so an *NA* value is entered for c<sub>1</sub>. In this case, it is known that the event must have happened sometime between (and inclusive of) *t* = 1 and *t* = 16.

The last row is an example of right censoring. In this case, *y* = 1 was never observed, so an *NA* value is entered for c<sub>2</sub>. In this case, it is known that the event must have happened sometime after *t* = 2.

## Model
The Bayesian model used in this package is described below.

<div align="center">
  <img src="/doc/images/model_math.png" width="250">
</div>

 **X**<sub>i</sub> is a *T* &times; *M* matrix of covariates for observation *i*. **X**<sub>i</sub>\[t,m\] is the value of covariate *m* at time *t* for observation *i*. **&beta;** is a 1 &times; *M* vector of regression coefficients. ***p***<sub>i</sub> is a *T*+1 &times; 1 vector of time-varying probabilities of event occurence for observation *i* for each time step, *t*. The last element in ***p***<sub>i</sub> is forced to be 1, which is required for the time-varying geometric distribution and represents the case where the event does not happen in the period of *t*=1 to *T*.
 
 *z*<sub>i</sub> represents the time step at which the event of interest occurred, but censoring prevents exact observations of *z*<sub>i</sub> in many cases (e.g. rows 2, 3, and 4 in the table above). For those observation subject to censoring, *z*<sub>i</sub> is imputed using a truncated time-varying geometric distribution (tvgeom). The lower and upper limits of the truncated distribution are defined by the cutpoints (c<sub>1</sub> and c<sub>2</sub>). The imputed value of *z*<sub>i</sub> can take on any value in the range (c<sub>1</sub>, c<sub>2</sub>\], according to the truncated tvgeom distribution with parameter ***p***<sub>i</sub>.
 
## Installation

This package is currently only hosted on GitLab. Installation requires the `devtools` package. Run the following code in R to install devtools and tempo. Package dependencies for tempo will be automatically installed.

```R
install.packages("devtools")
devtools::install_gitlab("actionable-phenology/tempo")
```
