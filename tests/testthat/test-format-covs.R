context("format covariates")
test_that(
  "testing covariate wrangling", {
    y <- tibble(obs_id = c(1:20), c1 = round(runif(20, 10, 30))) %>%
      mutate(c2 = c1 + round(runif(20, 1, 8)))
    covs_long <- tidyr::crossing(obs_id = c(1:20), time_step = c(1:100)) %>%
      mutate(covariate1 = rnorm(n = 2000),
             covariate2 = rnorm(n = 2000))

    X <- tempo_wrangle(covs_long, obs_ids = c(1:20), vars = c("covariate1", "covariate2"))

    expect_true(is.list(X))
    expect_equal(length(X), 2)
    expect_equal(dim(X[[1]]), c(20, 100))
  }
)
