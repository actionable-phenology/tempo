context("nimble input wrangling")

test_that(
  "testing nimble inputs wrangling", {
    data <- tempo_simulate(n = 100, t = 365,
                           censor_intensity = 3,
                           beta_mu = c(-5, 0.5, -1, 0.3),
                           random_betas = NULL,
                           beta_sd = NULL,
                           covariates = NULL,
                           n_group = NULL,
                           rho = NULL)

    # The following 7 lines are run at onset of tempo_mcmc(), so need to
    # run them prior to calling nimble_inputs
    data$y$c1[is.na(data$y$c1)] <- 0
    data$y$c2[is.na(data$y$c2)] <- 51
    data$covariates <- c(
      list(intercept = array(1, dim = dim(data$covariates[[1]]))),
           data$covariates
    )

    data$covariates <- do.call(abind, c(data$covariates, along = 3))

    inputs <- nimble_inputs(y = data$y,
                            covariates = data$covariates,
                            beta_prior_mean = rep(0, 4),
                            beta_prior_sd = rep(1.5, 4),
                            beta_start = rep(0, 4),
                            beta_random_indices = NULL,
                            group_ids = NULL,
                            sd_eps_start = NULL,
                            correlated = TRUE,
                            pp_checks = "mean")

    inputs2 <- nimble_inputs(y = data$y,
                             covariates = data$covariates,
                             beta_prior_mean = rep(0, 4),
                             beta_prior_sd = rep(1.5, 4),
                             beta_start = rep(0, 4),
                             beta_random_indices = c(1, 3),
                             group_ids = rep(1:10, each = 5),
                             sd_eps_start = c(0.01, 0.01),
                             correlated = TRUE,
                             pp_checks = c("mean", "variance"))
    expect_equal(inputs2$pheno_consts$beta_fixed_indices, c(2, 4))

    inputs3 <- nimble_inputs(y = data$y,
                             covariates = data$covariates,
                             beta_prior_mean = rep(0, 4),
                             beta_prior_sd = rep(1.5, 4),
                             beta_start = rep(0, 4),
                             beta_random_indices = c(1),
                             group_ids = rep(1:10, each = 5),
                             sd_eps_start = c(0.01, 0.01),
                             correlated = TRUE,
                             pp_checks = NA)
    expect_null(inputs3$pheno_consts$mean_eps)
  }
)
